<?php

/**
 * @file plugins/oaiMetadataFormats/rdfAgrovoc/index.php

 * @ingroup oai_format_rdfAgrovoc
 * @brief Wrapper for the OAI rdfAgrovoc format plugin.
 *
 */

require_once('OAIMetadataFormatPlugin_rdfAgrovoc.inc.php');
require_once('OAIMetadataFormat_rdfAgrovoc.inc.php');

return new OAIMetadataFormatPlugin_rdfAgrovoc();

?>
