<?php

/**
 * @file plugins/oaiMetadataFormats/rdfAgrovoc/OAIMetadataFormatPlugin_rdfAgrovoc.inc.php
 *
 * @class OAIMetadataFormatPlugin_rdfAgrovoc
 * @ingroup rdfAgrovoc
 * @see OAI
 *
 * @brief rdfAgrovoc Journal Article metadata format plugin for OAI.
 */

import('lib.pkp.classes.plugins.OAIMetadataFormatPlugin');

class OAIMetadataFormatPlugin_rdfAgrovoc extends OAIMetadataFormatPlugin {
	/**
	 * Get the name of this plugin. The name must be unique within
	 * its category.
	 * @return String name of plugin
	 */
	function getName() {
		return 'OAIMetadataFormatPlugin_rdfAgrovoc';
	}

	function getDisplayName() {
		return __('plugins.oaiMetadata.rdfAgrovoc.displayName');
	}

	function getDescription() {
		return __('plugins.oaiMetadata.rdfAgrovoc.description');
	}

	function getFormatClass() {
		return 'OAIMetadataFormat_rdfAgrovoc';
	}

	function getMetadataPrefix() {
		return 'rdfAgrovoc';
	}

	function getSchema() {
		return 'http://www.openarchives.org/OAI/2.0/rdf.xsd';
	}

	function getNamespace() {
		return 'http://www.w3.org/1999/02/22-rdf-syntax-ns#';
	}
}

?>
