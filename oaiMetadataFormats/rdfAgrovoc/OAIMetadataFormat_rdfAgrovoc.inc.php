<?php

/**
 * @defgroup oai_format_rdfAgrovoc
 */

/**
 * @file plugins/oaiMetadataFormats/rdfAgrovoc/OAIMetadataFormat_rdfAgrovoc.inc.php

 * @class OAIMetadataFormat_rdfAgrovoc
 * @ingroup oai_format
 * @see OAI
 *
 * @brief OAI metadata format class -- rdfAgrovoc 2.3
 */


class OAIMetadataFormat_rdfAgrovoc extends OAIMetadataFormat {

	/**
	 * @see OAIMetadataFormat#toXml
         * Article order in the issue's Table of Contents
	 */

        	function toXml(&$record, $format = null) {
		$article =& $record->getData('article');
		$journal =& $record->getData('journal');
		$section =& $record->getData('section');
		$issue =& $record->getData('issue');
		$galleys =& $record->getData('galleys');

		// Publisher
		$publisher = $journal->getLocalizedTitle(); // Default
		$publisherInstitution = $journal->getLocalizedSetting('publisherInstitution');
		if (!empty($publisherInstitution)) {
			$publisher = $publisherInstitution;
		}

		// Sources contains journal title, issue ID, and pages
		$source = $issue->getIssueIdentification();
		$pages = $article->getPages();
		if (!empty($pages)) $source .= '; ' . $pages;

		// Relation
		$relation = array();
		foreach ($article->getSuppFiles() as $suppFile) {
			$relation[] = Request::url($journal->getPath(), 'article', 'download', array($article->getId(), $suppFile->getFileId()));
		}

		// Format creators
		$creators = array();
		$authors = $article->getAuthors();
		for ($i = 0, $num = count($authors); $i < $num; $i++) {
			$authorName = $authors[$i]->getFullName(true);
			$affiliation = $authors[$i]->getLocalizedAffiliation();
			if (!empty($affiliation)) {
				$authorName .= '; ' . $affiliation;
			}
			$creators[] = $authorName;
		}

		// Subject
		$subjects = array_merge_recursive(
			$this->stripAssocArray((array) $article->getDiscipline(null)),
			$this->stripAssocArray((array) $article->getSubject(null)),
			$this->stripAssocArray((array) $article->getSubjectClass(null))
		);
		$subject = isset($subjects[$journal->getPrimaryLocale()])?$subjects[$journal->getPrimaryLocale()]:'';

		// Coverage
		$coverage = array(
			$article->getLocalizedCoverageGeo(),
			$article->getLocalizedCoverageChron(),
			$article->getLocalizedCoverageSample()
		);

                //loop for spearate parts of agrovoc terms
        for ($i = 0; $i < count($article->getData('agrovoc')); $i++) {
            $itemArray = explode("|", $article->getData('agrovoc')[$i], 3); //separamos los tres elementos

            $term[$i] = $itemArray[0];
            $id[$i] = $itemArray[1];
            $lang[$i] = $itemArray[2];

            $itemAgrovoc[$i] = 'http://aims.fao.org/aos/agrovoc/c_' . $id[$i]; //termino agrovoc                                                    
        }
                
		$url = Request::url($journal->getPath(), 'article', 'view', array($article->getBestArticleId()));
		$response = "<rdf:RDF\n" .
			"xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"\n" . 
                        "xmlns:dc=\"http://purl.org/dc/elements/1.1/\"\n" .
                        "xmlns:dcterms=\"http://purl.org/dc/terms/\"\n" . 
                        "xmlns:ags=\"http://www.fao.org/agris/agmes/schemas/1.0/ags#\"\n" .
                        "\txsi:schemaLocation=\"http://www.openarchives.org/OAI/2.0/rdf\n" .
			"\thttp://www.openarchives.org/OAI/2.0/rdf.xsd\">\n" .
                        "<rdf:Description>\n" .                   
                        $this->formatElement('title', $article->getLocalizedTitle()) .
                        $this->formatElement('creator', $creators) .
                        $this->formatElement('subject ', $subject) .
                        $this->formatElementAgrovoc('subject ', $itemAgrovoc) .
                        $this->formatElement('description', strip_tags($article->getLocalizedAbstract())) .
                        $this->formatElement('publisher', $publisher) .
                        $this->formatElement('contributor', $article->getLocalizedSponsor()) .
                        ($article->getDatePublished()?$this->formatElement('date', $article->getDatePublished()):'') .
                        $this->formatElement('type', $section->getLocalizedIdentifyType()) .
			$this->formatElement('type', $relation) .
			$this->formatElement('identifier', $url) .
                        $this->formatElement('source', $source) .
                        $this->formatElement('language', $article->getLanguage()) .
                        $this->formatElement('copyright', strip_tags($journal->getLocalizedSetting('copyrightNotice'))) .
			"</rdf:Description>\n" .
                        "</rdf:RDF>\n";

		return $response;
	}
        
        /**
	 * Format XML for single  element.
	 * @param $name string
	 * @param $value mixed
	 */
        function formatElement($name, $value) {
		if (!is_array($value)) {
			$value = array($value);
		}
		$response = '';
		foreach ($value as $v) {
			$response .= "\t<dc:$name>" . OAIUtils::prepOutput($v) . "</dc:$name>\n";
		}
		return $response;
	}
        
                
        //agrovoc format
	function formatElementAgrovoc($name, $value) {
		if (!is_array($value)) {
			$value = array($value);
		}
		$response = "\t<dc:$name>";
		foreach ($value as $v) {
			$response .= "<ags:subjectThesaurus scheme=\"ags:AGROVOC\">" . 
                                OAIUtils::prepOutput($v) . 
                                "</ags:subjectThesaurus>";
		}
                $response .= "</dc:$name>\n";
		return $response;
	}
        
        

}

?>
