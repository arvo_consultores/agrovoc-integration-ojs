1. Para utilizar el plugin es necesario habilitar soap en el servidor web utilizado:

Ejemplo utilizando apache en debian:

Instalamos soap ->
sudo apt-get install php-soap 

Habilitamos soap buscando y descoemntando en el siguiente fichero ->
vi /etc/php5/apache2/php.ini 
extension=php_soap.dll

Comprobamos que esta habilitado consultando en phpinfo ->
soap
Soap Client 	enabled
Soap Server 	enabled 


2. Para las busquedas en agrovoc se utiliza el siguiente servicio web:

http://artemide.art.uniroma2.it/SKOSWS/services/SKOSWS?wsdl

con sus metodos documentados aqui:
https://aims-fao.atlassian.net/wiki/spaces/AGV/pages/1507349/List+of+Web+Services


3. Instalacion:

//Para poder instalar plugins desde la interfaz web es necesario que la carpeta ojs/plugins(y su contenido) tenga permiso de escritura

//Instalamos plugin con usuario gestor de la revista
//Journal Management > Plugin Management > Install A New Plugin
//Seleccionams el fichero agrovoc.tar.gz