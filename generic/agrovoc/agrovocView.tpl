{**
* plugins/generic/agrovoc/agrovocView.tpl
*}
<!-- agrovoc -->
{if $submission->getData('agrovoc')!=''}
    <div id="openAIRE">
        <h4>{translate key="plugins.generic.agrovoc.agrovocTitle"}</h4>
        <table width="100%" class="data">
            <tr valign="top">
                <td rowspan="2" width="20%" class="label">{translate key="plugins.generic.agrovoc.agrovoc"}</td>
                <td width="80%" class="value">
                    {foreach from=$submission->getData('agrovoc') item=agrovoc}
                        {$agrovoc|escape|default:"&mdash;"}<br> 
                    {/foreach}
                </td>
            </tr>
        </table>
    </div>
{/if}
<!-- /agrovoc -->