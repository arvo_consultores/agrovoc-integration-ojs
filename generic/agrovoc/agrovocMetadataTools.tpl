{**
* plugins/generic/agrovoc/agrovocArticle.tpl
*}
<!-- agrovoc -->
{if $article->getData('agrovoc')!=''}
    <div class="agrovoc"> 
        <br>
        <h4 style="color: #003333; font-weight: bold;font-size: 15px;border-bottom: 1px #003300 solid;">
            {translate key="plugins.generic.agrovoc.agrovocTitle"}
        </h4>
        <table class="listing" width="100%">
            <tr valign="top">
                <td class="heading" width="27%" colspan="2">{translate key="rt.metadata.dublinCore"}</td>
                <td class="heading" width="25%">{translate key="rt.metadata.pkpItem"}</td>
                <td class="heading" width="50%">{translate key="rt.metadata.forThisDocument"}</td>
            </tr>
            <tr><td colspan="4" class="headseparator">&nbsp;</td></tr>
            {section name=agrovocPart loop=$term}
                <tr valign="top">
                    <td width ='3%'></td>
                    <td>{translate key="rt.metadata.dublinCore.subject"}</td>
                    <td>{translate key="plugins.generic.agrovoc.agrovocMetadataTools"}</td>
                    <td><a href="http://aims.fao.org/aos/agrovoc/c_{$id[agrovocPart]}" target="_blank">{$term[agrovocPart]} ({$lang[agrovocPart]})</a></td>
                </tr>
                <tr><td colspan="4" class="separator">&nbsp;</td></tr>
            {/section}
        </table>
        <br>
    </div> 
{/if}


{literal}    
    <script type="text/javascript">
        $('.agrovoc').insertBefore('input');
    </script> 
{/literal}
<!-- /agrovoc -->