<?php
$client = new SoapClient('http://artemide.art.uniroma2.it/SKOSWS/services/SKOSWS?wsdl'); //url servicio

$parametros['searchString']=$_GET['search']; //cadena
$parametros['searchmode']="starting"; //condicion (starting, containing)
$parametros['separator']="||"; //separador

$result = $client->simpleSearchByMode2($parametros);//llamamos al métdo que nos interesa con los parámetros

//substring para quitar [ , explode para separar
$resultadoSeparado = explode("||", substr($result->simpleSearchByMode2Return, 1));

//bucle para separar de 3 en 3
for($i=0;$i<count($resultadoSeparado)-2;$i+=3){
    $id=$resultadoSeparado[$i];
    $term=$resultadoSeparado[$i+1];
    $lang=$resultadoSeparado[$i+2];
    $data[]= $term . '|' . $id . '|' . $lang;
}

//return json data
echo json_encode($data);


