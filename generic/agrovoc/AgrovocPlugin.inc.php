<?php

/**
 * @file plugins/generic/agrovoc/AgrovocPlugin.inc.php
 * @class AgrovocPlugin
 * @ingroup plugins_generic_agrovoc
 */
import('lib.pkp.classes.plugins.GenericPlugin');

class AgrovocPlugin extends GenericPlugin {

    function getDisplayName() {
        return __('plugins.generic.agrovoc.displayName');
    }

    function getDescription() {
        return __('plugins.generic.agrovoc.description');
    }

    /* Called as a plugin is registered to the registry
     * @param $category String Name of category plugin was registered to
     * @return boolean True if plugin initialized successfully; if false,
     * 	the plugin will not be registered. */

    function register($category, $path) {
        $success = parent::register($category, $path);
        if (!Config::getVar('general', 'installed') || defined('RUNNING_UPGRADE'))
            return true;
        if ($success && $this->getEnabled()) {

            // Insert new field into author metadata submission form (submission step 3) and metadata form 
            HookRegistry::register('Templates::Author::Submit::AdditionalMetadata', array($this, 'metadataFieldEdit'));
            HookRegistry::register('Templates::Submission::MetadataEdit::AdditionalMetadata', array($this, 'metadataFieldEdit'));
            // Consider the new field in the metadata view
            HookRegistry::register('Templates::Submission::Metadata::Metadata::AdditionalMetadata', array($this, 'metadataFieldView'));

            // Hook for initData in two forms -- init the new field (show values in edit form)
            HookRegistry::register('metadataform::initdata', array($this, 'metadataInitData'));
            HookRegistry::register('authorsubmitstep3form::initdata', array($this, 'metadataInitData'));

            // Hook for readUserVars in two forms -- consider the new field entry (read var from form)
            HookRegistry::register('metadataform::readuservars', array($this, 'metadataReadUserVars'));
            HookRegistry::register('authorsubmitstep3form::readuservars', array($this, 'metadataReadUserVars'));


            // Hook for execute in two forms -- consider the new field in the article settings
            HookRegistry::register('authorsubmitstep3form::execute', array($this, 'metadataExecute'));
            HookRegistry::register('metadataform::execute', array($this, 'metadataExecute'));

            // Consider the new field for ArticleDAO for storage (save into bd)
            HookRegistry::register('articledao::getAdditionalFieldNames', array($this, 'articleSubmitGetFieldNames'));


            // Add agrovoc metadata to article landing page
            HookRegistry::register('Templates::Article::MoreInfo', array($this, 'insertInfo'));

            // Add agrovoc metadata to Indexing metadata tools
            HookRegistry::register('Templates::Rt::Footer::PageFooter', array($this, 'insertInfoMetadataTools'));

            // // Change Dc11Desctiption -- dc.subject = item agrovoc
            HookRegistry::register('Dc11SchemaArticleAdapter::extractMetadataFromDataObject', array($this, 'changeDc11Desctiption'));
        }

        return $success;
    }

    //Insert agrovoc field into author submission step 3 and metadata edit form
    function metadataFieldEdit($hookName, $params) {
        $smarty = & $params[1];
        $output = & $params[2];

        //use template agrovocEdit.tpl to add html to form
        $output .= $smarty->fetch($this->getTemplatePath() . 'agrovocEdit.tpl');
        return false;
    }

    // Add agrovoc to the metadata view
    function metadataFieldView($hookName, $params) {
        $smarty = & $params[1];
        $output = & $params[2];

        //use template agrovocView.tpl to view metadata
        $output .= $smarty->fetch($this->getTemplatePath() . 'agrovocView.tpl');
        return false;
    }

    //Set article agrovoc metadata 
    function metadataExecute($hookName, $params) {
        $form = & $params[0];
        $article = & $form->article;
        $formAgrovoc = $form->getData('agrovoc');

        //guardamos en la bd (vacio si no hay nada en el form)
        if (!is_null($formAgrovoc)) { //validamos si el form va vacio
            $cont = 0; //contador pra validar items nulos

            for ($i = 0; $i < count($formAgrovoc); $i++) { //bucle para separar items
                $itemArray = explode("|", $formAgrovoc[$i], 3); //separamos lso 3 elementos
                $term[$i] = $itemArray[0];    //asignamos termino                          
                $lang[$i] = $itemArray[2]; //asiganmos idioma

                try { //intentamos conectar con agrovoc
                    if (class_exists('SoapClient')) { //comprobamos si soap esta habilitado
                        //soap query to intruduce id for links
                        $client = new SoapClient('http://artemide.art.uniroma2.it/SKOSWS/services/SKOSWS?wsdl'); //url servicio
                        //consulta comprobar id------------------------------------------------
                        $parametros2['termCode'] = $itemArray[1]; //cadena
                        $result2 = $client->getConceptInfoByTermcode($parametros2); //llamamos al métdo que nos interesa con los parámetros
                        $idTermSup = explode(", ", substr($result2->getConceptInfoByTermcodeReturn[3], 0, -1));
                        //end soap query
                    } else {
                        $idTermSup = ""; //id vacio
                    }
                } catch (Exception $e) {
                    $idTermSup = ""; //id vacio
                }//intentamos coenctar con agrovoc

                if ($idTermSup[1] != "") { //valdamos i el id es el superior o no
                    $id[$i] = $idTermSup[1];
                } else {
                    $id[$i] = $itemArray[1];
                }

                if (!is_null($term[$i]) && !is_null($id[$i]) && !is_null($lang[$i])) { //validamos si el termino es correcto
                    $formAgrovocMod[$cont] = $term[$i] . '|' . $id[$i] . '|' . $lang[$i];
                    $cont++;
                }
            }

            $article->setData('agrovoc', $formAgrovocMod); //gaba en la bd
        } else {
            $article->setData('agrovoc', ''); //item vacio
        }
        return false;
    }

    //Init agrovoc metadata 
    function metadataInitData($hookName, $params) {
        $form = & $params[0];
        $article = & $form->article;
        $articleAgrovoc = $article->getData('agrovoc');
        $form->setData('agrovoc', $articleAgrovoc);
        return false;
    }

    //Add agrovoc metadata  element to the article
    function articleSubmitGetFieldNames($hookName, $params) {
        $fields = & $params[1];
        $fields[] = 'agrovoc';
        return false;
    }

    //Concern agrovoc field in the form
    function metadataReadUserVars($hookName, $params) {
        $userVars = & $params[1];
        $userVars[] = 'agrovoc';
        return false;
    }

    //Insert agrovoc metadata article page
    function insertInfo($hookName, $params) {
        // if ($this->getEnabled()) {
        $smarty = & $params[1];
        $output = & $params[2];
        $templateMgr = & TemplateManager::getManager();

        //array agrovoc terms
        $agrovocArray = $templateMgr->get_template_vars('article')->getData('agrovoc');

        //loop for spearate parts of agrovoc terms
        for ($i = 0; $i < count($agrovocArray); $i++) {
            $itemArray = explode("|", $agrovocArray[$i], 3); //separamos los tres elementos

            $term[$i] = $itemArray[0];
            $id[$i] = $itemArray[1];
            $lang[$i] = $itemArray[2];
        }

        //include in tpl var term, id, lang
        $smarty->assign('term', $term);
        $smarty->assign('id', $id);
        $smarty->assign('lang', $lang);

        $output .= $smarty->fetch($this->getTemplatePath() . 'agrovocArticle.tpl');
        // }
        return false;
    }

    //Insert agrovoc metadata indexing metadata tools page
    function insertInfoMetadataTools($hookName, $params) {
        if ($this->getEnabled()) {
            $smarty = & $params[1];
            $output = & $params[2];
            $templateMgr = & TemplateManager::getManager();

            //array agrovoc terms
            $agrovocArray = $templateMgr->get_template_vars('article')->getData('agrovoc');

            //loop for spearate parts of agrovoc terms
            for ($i = 0; $i < count($agrovocArray); $i++) {
                $itemArray = explode("|", $agrovocArray[$i], 3); //separamos los tres elementos

                $term[$i] = $itemArray[0];
                $id[$i] = $itemArray[1];
                $lang[$i] = $itemArray[2];
            }

            //include in tpl var term, id, lang
            $smarty->assign('term', $term);
            $smarty->assign('id', $id);
            $smarty->assign('lang', $lang);

            $output .= $smarty->fetch($this->getTemplatePath() . 'agrovocMetadataTools.tpl');
        }
        return false;
    }

    function getAgrovocPart($agrovocArray) {
        //loop for spearate parts of agrovoc terms
        for ($i = 0; $i < count($agrovocArray); $i++) {
            $itemArray = explode("|", $agrovocArray[$i], 3); //separamos los tres elementos

            $term[$i] = $itemArray[0];
            $id[$i] = $itemArray[1];
            $lang[$i] = $itemArray[2];

            $agrovocArrayNew[$i] = array($term[$i], $id[$i], $lang[$i]);
        }

        return $agrovocArrayNew;
    }

    function changeDc11Desctiption($hookName, $params) {
        $adapter = & $params[0];
        $article = $params[1];
        $journal = $params[2];
        $issue = $params[3];
        $dc11Description = & $params[4];

        //loop for spearate parts of agrovoc terms
        for ($i = 0; $i < count($article->getData('agrovoc')); $i++) {
            $itemArray = explode("|", $article->getData('agrovoc')[$i], 3); //separamos los tres elementos

            $term[$i] = $itemArray[0];
            $id[$i] = $itemArray[1];
            $lang[$i] = $itemArray[2];

            $itemAgrovoc[$i] = $term[$i]; //termino agrovoc                                                    
        }
        //$dcItemAgrovoc[$lang[0].'-'.strtoupper($lang[0])] = array($itemAgrovoc); //añadimos idioma

        if ($dc11Description->hasStatement('dc:subject')) {
            $dcSubjectOrig = $dc11Description->getStatement('dc:subject'); //recogemos subject original
        }
        $dcSubjectNew = array_merge($dcSubjectOrig, $itemAgrovoc); //fusionamos arrays
        $newSubject = array('dc:subject' => $dcSubjectNew); //creamos nuevo elemento                                             
        $dc11Description->setStatements($newSubject); //añadimos elemnto a interface



        return false;
    }

}

?>
