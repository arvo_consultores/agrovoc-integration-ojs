{**
* plugins/generic/agrovoc/agrovocEdit.tpl
*}
<!-- agrovoc -->
<div id="agrovoc">
    <h3>{translate key="plugins.generic.agrovoc.agrovocTitle"}</h3>
    <table width="100%" class="data">
        <tr valign="top">
            <td width="20%" class="label">
                {fieldLabel name="agrovoc" key="plugins.generic.agrovoc.agrovoc"}
                {translate key="plugins.generic.agrovoc.agrovocExample"}
            </td>
        </tr>
        <tr valign="top">
            <td width="80%" class="value">
                <div id="dynamicInput">
                    <!--variable contador para generear input numericos -->
                    {assign var="contador" value=1}
                    <!--bucle para recorrer elementos de agrovoc guardaos en la bd -->
                    {foreach from=$agrovoc item=agrovoc}
                        <div class="agrovocInput">
                            <input type="text" required class="miInput" id="miInput_{$contador++}" name="agrovoc[]" value="{$agrovoc|escape}" size="50" maxlength="100"/>
                        </div>
                    {/foreach}
                </div>
                <!-- llamada a funciones apra añadir/quitar elementos -->
                <input type="button" value="{translate key="plugins.generic.agrovoc.agrovocAdd"}" onClick="addInput('dynamicInput');">
                <input type="button" value="{translate key="plugins.generic.agrovoc.agrovocRemove"}" onClick="removeElement('agrovocInput');">
            </td>
        </tr>
    </table>
</div>
<div class="separator"></div>

{literal}    
    <!--scripts necesarios para añadir/quitar elementos y usar ajax -->
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script type="text/javascript" src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <!--script para añadir ajax en cada busqueda de elemento-->
    <script type="text/javascript">
        $(document).ready(function () {
            $(document).on('keydown', '.miInput', function () {
                var id = this.id;
                var splitid = id.split('_');
                var index = splitid[1];
                $('#' + id).autocomplete({
                    source: function (request, response) {
                        //url fichero autocomplete.php clase que hace las busquedas
                        $.ajax({
                            url: "../../../../plugins/generic/agrovoc/autocomplete.php",
                            type: 'get',
                            dataType: "json",
                            data: {
                                search: request.term, request: 1
                            },
                            success: function (data) {
                                response(data);
                            }
                        });
                    },
                    select: function (event, ui) {
                        $(this).val(ui.item.label); // display the selected text
                        var userid = ui.item.value; // selected id to input
                        //url fichero autocomplete.php clase que hace las busquedas
                        $.ajax({
                            url: '../../../../plugins/generic/agrovoc/autocomplete.php',
                            type: 'get',
                            data: {userid: userid, request: 2},
                            dataType: 'json',
                            success: function (response) {
                            }
                        });
                        return false;
                    }
                });
            });
        });
    </script>
    <!--funciones apara añadir/quitar elementos -->
    <script type="text/javascript">
        function addInput(divName) {

            if ($('#dynamicInput input[type=text]:nth-child(1)').length < 1) {
                //si no hay id creamos el 1
                lastname_id = "miInput_1";
            } else {
                //obtenemos el ultimo id
                lastname_id = $('#dynamicInput input[type=text]:nth-child(1)').last().attr('id');
            }
            var split_id = lastname_id.split('_');
            // New index
            var index = Number(split_id[1]) + 1;
            // Create row with input elements                   
            var newdiv = document.createElement('div');
            newdiv.setAttribute('class', 'agrovocInput');
            newdiv.innerHTML = "<input type='text' required class='miInput' id='miInput_" + index + "' name='agrovoc[]' size='50' maxlength='100'>";
            document.getElementById(divName).appendChild(newdiv);
        }
        function removeElement(divName) {
            $("." + divName + ":last-child").remove();
        }
    </script> 
{/literal}
<!-- /agrovoc -->