{**
* plugins/generic/agrovoc/agrovocArticle.tpl
*}
<!-- agrovoc -->
{if $article->getData('agrovoc')!=''}
    <div id="agrovoc">

        <h4 style="color: #003333; font-weight: bold;font-size: 15px;border-bottom: 1px #003300 solid;">
            {translate key="plugins.generic.agrovoc.agrovocTitle"}
        </h4><br>
        <div>
            {section name=agrovocPart loop=$term}
                <a href="http://aims.fao.org/aos/agrovoc/c_{$id[agrovocPart]}" target="_blank">{$term[agrovocPart]} ({$lang[agrovocPart]})</a>
                <br>
            {/section}
            <br>
        </div>
    </div>
{/if}


{literal}    
    <script type="text/javascript">
        if ($('#articleFullText').length > 0) {
            $('#agrovoc').insertBefore('#articleFullText');
        } else {
            if ($('#articleCitations').length > 0) {
                $('#agrovoc').insertBefore('#articleCitations');
            }
        }
    </script> 
{/literal}
<!-- /agrovoc -->